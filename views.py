from django.shortcuts import render

from .models import classEntry, userEntry, assignmentEntry, completeEntry

def assignmentSort(student):
    return student['assignment_id']

def bri(request):
    classes = classEntry.objects.all()
    return render(request, 'bri/bri.html', {'classes': classes})

def allClasses(request):
    return render(request, 'bri/classes.html')

def completionView(request, class_group='None'):
    #Gather relavent data from db
    users = userEntry.objects.filter(class_group=class_group).order_by('user')
    assignments = assignmentEntry.objects.filter(class_group=class_group).order_by('-id')
    completes = completeEntry.objects.filter(
            user__class_group=class_group).filter(
                    assignment__class_group=class_group).values(
            'id', 'user__user', 'assignment__title', 'completed')
    
    #If data is posted by client, check if data is valid, then update db
    if request.method == 'POST' and request.user.is_staff:
        ids = list(completes.values_list('id', flat=True))
        objs = []
        for k, v in request.POST.items():
            try:
                if int(k) in ids:
                    completes.filter(id=k).update(completed=v)
            except:
                pass

    #Build table from the above fields
    chart = [['']]
    counter = 0
    for a in assignments:
        chart[0].append(a.title)
    for u in users:
        counter += 1
        chart.append([])
        for i in completes.values():
            if i['user_id'] == u.id:
              chart[counter].append(i)
        chart[counter].sort(key=assignmentSort, reverse=True)
        chart[counter].insert(0, u.user)
    return render(request, 'bri/compChart.html', {'chart': chart})
