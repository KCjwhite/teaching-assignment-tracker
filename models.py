import datetime

from django.db import models
from django.utils import timezone
from django.utils.text import slugify
from multiselectfield import MultiSelectField

########################
### Main site models ###
########################

class classEntry(models.Model):
    teacher = models.CharField(max_length=50)
    class_group = models.CharField(max_length=50)

    def __str__(self):
        return self.class_group

class userEntry(models.Model):
    user = models.CharField(max_length=10)
    class_group = models.ForeignKey(classEntry, on_delete=models.CASCADE)
    description = models.TextField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True, max_length=80)
    parent_email = models.EmailField(null=True, blank=True, max_length=80)
    
    def __str__(self):
        return self.user

    def save(self, *args, **kwargs):
        if self._state.adding:
            try:
                assignments = assignmentEntry.objects.all()
                super(userEntry, self).save(*args, **kwargs)
                for a in assignments:
                    if self.class_group == a.class_group:
                        completeEntry.objects.create(user=self, assignment=a)
            except:
                print('Problem connecting new user to assignments')
        else:
            super(userEntry, self).save(*args, **kwargs)


class assignmentEntry(models.Model):
    title = models.CharField(max_length=20)
    class_group = models.ForeignKey(classEntry, on_delete=models.CASCADE)
    description = models.TextField(null=True, blank=True)
    assigned_date = models.DateField(verbose_name='assigned date')
    due_date = models.DateField(verbose_name='due date')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self._state.adding:
            try:
                users = userEntry.objects.all()
                super(assignmentEntry, self).save(*args, **kwargs)
                for u in users:
                    if self.class_group == u.class_group:
                        completeEntry.objects.create(user=u, assignment=self)
            except:
                print('Problem connecting new assignment to users')
        else:
            super(assignmentEntry, self).save(*args, **kwargs)



class completeEntry(models.Model):
    user = models.ForeignKey(userEntry, on_delete=models.CASCADE)
    assignment = models.ForeignKey(assignmentEntry, on_delete=models.CASCADE)
    completed = models.BooleanField(default=False)

    def __str__(self):
        return self.user.user + '/' + self.assignment.title
