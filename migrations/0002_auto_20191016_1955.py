# Generated by Django 2.1.1 on 2019-10-16 19:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bri', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='assignmententry',
            name='user',
        ),
        migrations.AddField(
            model_name='completiontable',
            name='user',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='bri.userEntry'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='assignmententry',
            name='image',
            field=models.URLField(blank=True, max_length=250, null=True),
        ),
        migrations.AlterField(
            model_name='userentry',
            name='email',
            field=models.EmailField(blank=True, max_length=80, null=True),
        ),
        migrations.AlterField(
            model_name='userentry',
            name='image',
            field=models.URLField(blank=True, max_length=250, null=True),
        ),
    ]
