from django.contrib import admin

from .models import classEntry, userEntry, assignmentEntry

class userAdmin(admin.ModelAdmin):
    list_filter = ['class_group']

class assignmentAdmin(admin.ModelAdmin):
    list_filter = ['class_group', 'assigned_date', 'due_date']

admin.site.register(classEntry)
admin.site.register(userEntry, userAdmin)
admin.site.register(assignmentEntry, assignmentAdmin)


