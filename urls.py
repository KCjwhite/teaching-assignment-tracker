from django.urls import path

from . import views

app_name = 'bri'

urlpatterns = [
    path('', views.bri, name='bri'),
    path('completed/<str:class_group>/', views.completionView, name='compView')
]
